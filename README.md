# Max's dmenu-4.9 fork

Here lies my fork of dmenu. The patches I sometimes use are in `patches/`. To be honest, for dmenu I don't use many of the patches, but I've included the ones I've used in the past.

The rest of this README is the one provided with the standard distribution of `dmenu-4.9`.

dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.


Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
